{literal}
(function($) {
  $('div#customData').on('crmLoad', function() {
    // Don't want it to run right away because the default custom data block that applies to all case types loads first and triggers this.
    if ($('#case_type_id').hasClass('processed-newcopy')) {
      {/literal}{$jscode.code}{literal}
      // Don't want our code to run again if they change the case type manually.
      $('#case_type_id').removeClass('processed-newcopy');
    } else {
      $('#case_type_id').addClass('processed-newcopy').val('{/literal}{$jscode.case_type_id}{literal}').change();
    }
  });
})(CRM.$);
{/literal}
