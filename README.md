# saveandnewcopy

If you're creating a lot of new cases in a row that are mostly the same, this provides a "Save and New Copy" button to reduce typing.

The extension is licensed under [MIT](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM 5.19+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://gitlab.com/physician-health-program-of-bc/saveandnewcopy/-/archive/1.1/saveandnewcopy-1.1.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://gitlab.com/physician-health-program-of-bc/saveandnewcopy.git
cv en saveandnewcopy
```

## Other

A generic case import from a .csv file might be an alternative, but might be more complex for users.
