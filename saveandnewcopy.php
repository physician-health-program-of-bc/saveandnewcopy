<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

require_once 'saveandnewcopy.civix.php';
use CRM_Saveandnewcopy_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function saveandnewcopy_civicrm_config(&$config) {
  _saveandnewcopy_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function saveandnewcopy_civicrm_xmlMenu(&$files) {
  _saveandnewcopy_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function saveandnewcopy_civicrm_install() {
  _saveandnewcopy_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function saveandnewcopy_civicrm_postInstall() {
  _saveandnewcopy_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function saveandnewcopy_civicrm_uninstall() {
  _saveandnewcopy_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function saveandnewcopy_civicrm_enable() {
  _saveandnewcopy_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function saveandnewcopy_civicrm_disable() {
  _saveandnewcopy_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function saveandnewcopy_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _saveandnewcopy_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function saveandnewcopy_civicrm_managed(&$entities) {
  _saveandnewcopy_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function saveandnewcopy_civicrm_caseTypes(&$caseTypes) {
  _saveandnewcopy_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function saveandnewcopy_civicrm_angularModules(&$angularModules) {
  _saveandnewcopy_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function saveandnewcopy_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _saveandnewcopy_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function saveandnewcopy_civicrm_entityTypes(&$entityTypes) {
  _saveandnewcopy_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function saveandnewcopy_civicrm_themes(&$themes) {
  _saveandnewcopy_civix_civicrm_themes($themes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *
function saveandnewcopy_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function saveandnewcopy_civicrm_navigationMenu(&$menu) {
  _saveandnewcopy_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _saveandnewcopy_civix_navigationMenu($menu);
} // */

function saveandnewcopy_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Case_Form_Case' && $form->_action == CRM_Core_Action::ADD) {
    $form->addButtons(
      array(
        array(
          'type' => 'upload',
          'name' => ts('Save'),
          'isDefault' => TRUE,
        ),
        array(
          'type' => 'upload',
          'name' => ts('Save and New'),
          'subName' => 'new',
        ),
        array(
          'type' => 'upload',
          'name' => ts('Save and New Copy'),
          'subName' => 'newcopy',
          'icon' => 'fa-copy',
        ),
        array(
          'type' => 'cancel',
          'name' => ts('Cancel'),
        ),
      )
    );

    // buildForm gets called while saving too, but we don't want to do the following stuff while saving.
    if (!empty($form->_submitValues)) {
      return;
    }

    // Add the javascript that attaches to the new copy button to store the
    // form values. It seems to work better doing it javascripty than in
    // postProcess, since we're going to be setting the values by javascript
    // later, so we're fighting apples with apples.
    CRM_Core_Resources::singleton()->addScriptFile('saveandnewcopy', 'js/newCopy.js');

    // Check if we have some values to preset on this form
    $session = CRM_Core_Session::singleton();
    $userID = $session->get('userID');
    $jscode = \Civi::contactSettings($userID)->get('saveandnewcopy');
    if (!empty($jscode)) {
      // clear out value
      \Civi::contactSettings($userID)->set('saveandnewcopy', NULL);
      // add the javascript to set the field values
      $templateVars = array(
        'jscode' => $jscode,
      );
      $js = $form->getTemplate()->fetchWith('CRM/Saveandnewcopy/Case/Form/newCopyFill.js.tpl', $templateVars);
      CRM_Core_Resources::singleton()->addScript($js);
    }
  }
}

function saveandnewcopy_civicrm_postProcess($formName, &$form) {
  if ($formName == 'CRM_Case_Form_Case' && $form->_action == CRM_Core_Action::ADD) {
    $session = CRM_Core_Session::singleton();
    $buttonName = $form->controller->getButtonName();
    if ($buttonName == $form->getButtonName('upload', 'newcopy')) {
      // Do same thing that save and new button does.
      if ($form->_context == 'standalone') {
        $session->replaceUserContext(CRM_Utils_System::url('civicrm/case/add',
          "reset=1&action=add&context=standalone"
        ));
      }
      else {
        $session->replaceUserContext(CRM_Utils_System::url('civicrm/contact/view/case',
          "reset=1&action=add&context=case&cid={$form->_contactID}"
        ));
      }
    }
  }
}
