<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

use CRM_Saveandnewcopy_ExtensionUtil as E;

class CRM_Saveandnewcopy_Page_AJAX {

  public static function storejs() {
    $ret = ['is_error' => 0];
    if (!empty($_POST['jscode'])) {
      $js = $_POST['jscode'];
      $session = CRM_Core_Session::singleton();
      \Civi::contactSettings($session->get('userID'))->set('saveandnewcopy', $js);
    }
    CRM_Utils_JSON::output($ret);
  }

}
