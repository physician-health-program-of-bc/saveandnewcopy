(function($) {
  var buildCode = function() {
  // Build a string that is itself javascript code by looping through the fields and getting their current values and creating the code that would set that field's value.
    var s = '';
    $('div.crm-case-form-block').find('input[type="text"], input[type="number"], textarea, select').each(function() {
      var t = $(this);
      var v = t.val();
      // Note we skip case_type_id because it has to be done separately because it might change the custom fields. See below where it is sent separately in the ajax call, and then see the fill template where it's used separately.
      if (v != "" && this.id != 'case_type_id') {
        if (this.id == 'activity_details') {
          s += 'CKEDITOR.instances["activity_details"].setData(' + JSON.stringify(v) + ');' + "\n";
        } else {
          s += '$(' + JSON.stringify('#' + this.id) + ').val(' + JSON.stringify(v) + ').change();' + "\n";
        }
      }
    });
    $('div.crm-case-form-block').find('input[type="radio"], input[type="checkbox"]').each(function() {
      if (this.id != '') {
        var v = 'false'; // note string not boolean
        if (this.checked) {
          v = 'true';
        }
        s += 'document.getElementById(' + JSON.stringify(this.id) + ').checked = ' + v + ';' + "\n";
      }
    });
    return s;
  };

  // Attach to save and new copy button
  $('input[name="_qf_Case_upload_newcopy"], button[name="_qf_Case_upload_newcopy"]').click(function() {
    jscode = buildCode();
    $.ajax(
      CRM.url('civicrm/case/saveandnewcopy'),
      {
        data: {
          'jscode': {
            'code': jscode,
            'case_type_id': $('#case_type_id').val()
          }
        },
        dataType: 'text',
        type: 'POST',
        async: false,
        success: function (data) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(textStatus);
          console.log(errorThrown);
        }
      }
    );
  });
})(CRM.$);
